// ==UserScript==
// @name     joyreactor_feed.js
// @namespace https://gitlab.com/iria_somobu/monkey-scripts/
// @updateURL https://gitlab.com/iria_somobu/monkey-scripts/-/raw/master/joyreactor_feed.user.js
// @description Feed mode for joyreactor
// @include *joyreactor.cc/* 
// @include *.reactor.cc/*
// @grant   none
// @version 1
// @author  Iria.Somobu
// ==/UserScript==

let scroll_treshold = 0.85
let max_content_nodes = 100

var parser = null
var next_url = null

let content_root = document.getElementById('post_list')
let loading_indicator = document.createElement('div')

var script_enabled = false
var fire_scroll = false


if(content_root != null) {
  
  // Assign proper values to variables
  parser = new DOMParser()
  next_url = "http://" + document.domain + "/" + document.querySelector('.next').attributes['href'].value;
  
  // Add loading indicator at the bottom of the page (initially hidden)
  document.getElementById('content').appendChild(loading_indicator)
  
  // Also add feed button at the bottom
  let btn_next = document.createElement('button')
  btn_next.innerHTML = '<b>Го лентой</b>'
  btn_next.addEventListener('click', function() { loadNext(); })
  document.getElementById('content').appendChild(btn_next)
  
  // Subscribe on scroll event for auto-preload
  document.addEventListener('scroll', function(e) {
		if (fire_scroll && window.scrollY / window.scrollMaxY > scroll_treshold) loadNext();
  })
}



function getDividerNode(pageNum) {
	let dat_div = document.createElement('div')
  
  dat_div.classList.add('feed_page_divider')
  dat_div.innerHTML = '<h3>Страница '+pageNum + '</h3>'
  
  return dat_div
}


function markLoading(isLoading) {
	if(isLoading) loading_indicator.innerHTML = 'Грузимсо'
  else loading_indicator.innerHTML = ''
  
  fire_scroll = !isLoading;
}


function loadNext() {
  markLoading(true)
  
  console.log('Loading page '+next_url)
	fetch(next_url)
    .then(response => response.text())
  	.then(html => {
    	let doc = parser.parseFromString(html, "text/html")
      let pagename = doc.querySelector('.next').attributes['href'].value.substring(1)
			let content_root = doc.getElementById('post_list')
      let pagenum = pagename - 1
      if (isNaN(pagenum)) pagenum = "какая к черту разница";
      
      
      next_url = "http://" + document.domain + "/" + pagename;
    	appendContentRoot(content_root, pagenum)
    	markLoading(false)
    
    	doc.remove()
  	});
}


function appendContentRoot(cr, pagenum){
  
  content_root.appendChild(getDividerNode(pagenum))
  
  // Add new page at feed bottom
  for(child of cr.childNodes) {
  	content_root.appendChild(child)
  }
  
  // Remove excessive posts at feed top
  if(content_root.childNodes.length > max_content_nodes){
  	for(i = 0; i < max_content_nodes - content_root.childNodes.length; i++) {
      var c = content_root.firstChild
			content_root.removeChild(c)
      c.remove()
    }
  }
}

